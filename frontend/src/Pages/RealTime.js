import React from 'react';

import './RealTime.css';

export default class FetchDemo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cot: {
                name: "",
                buy: "",
                sell: "",
                variation: "",
                cotDate: ""
            }
        };
        this.append = this.append.bind(this);
    }

    componentDidMount() {
        console.log('Entrou');
        fetch('http://localhost/')
        .then(results => {
            return results.json();
        }).then(resultado => {
            this.setState({cot: resultado})
        }).then(() => {
            console.log('onLoad antes de RealTime')
            window.addEventListener('load', this.append) 
        });
    }

    append = async function() {
        console.log('append')
        var arr = this.state.cot;

        for(var item of arr) {
            var name = item.name;
            var buy = item.buy;
            var sell = item.sell;
            var variation = item.variation;
            var date = new Date(item.cotDate);
            var days = date.getDate();
            days = days < 10 ? days = '0' + days: days; 
            var month = date.getMonth() + 1;
            month = month < 10 ? month = '0' + month: month;
            var hours = date.getHours();
            hours = hours < 10 ? hours = '0' + hours: hours;
            var min = date.getMinutes();
            min = min < 10 ? min = '0' + min: min;
            var strDate = days + '/' + month + ' ' + hours + ':' + min; 

            var row = 
                `<tr>
                    <td>
                        <p>${name}</p>
                    </td>
                    <td>
                        <p>${buy}</p>
                    </td>
                    <td>
                        <p>${sell}</p>
                    </td>
                    <td>
                        <p>${variation}</p>
                    </td>
                    <td>
                        <p>${strDate}</p>
                    </td>
                </tr>`;
            document.getElementById("tbody").innerHTML += row;
        }
    }

    render() {
        return(
            <section className="header-rtime">
                <div className="box">
                    <div className="divHead">
                        <div className="fixed-head">
                            <header className="subHeader">
                                <h1 className="subTitle">Cotação Dólar para Real</h1>
                            </header>
                            <table className="table-cotationH">
                                <tbody>
                                    <tr className="tr-header">
                                        <td onClick={this.append}>
                                            <p>Moeda</p>
                                        </td>
                                        <td>
                                            <p>Preço Compra</p>
                                        </td>
                                        <td>
                                            <p>Preço Venda</p>
                                        </td>
                                        <td>
                                            <p>Variação</p>
                                        </td>
                                        <td>
                                            <p>Hora</p>
                                        </td>
                                    </tr>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="content">
                        <table id="cotation" className="table-cotationC">
                            <tbody id="tbody">

                            </tbody>
                        </table>
                </div>
            </div>
            </section>
        )
    }
}