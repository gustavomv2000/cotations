import React from 'react';

import './Footer.css';

export default function Footer() {
    return(
    <section>
        <div className="footer">
            <p className="left">Created by Gustavo Morales</p>
            <p className="right">Dados coletados de HgBrasil</p>
        </div>
    </section>
    )
}