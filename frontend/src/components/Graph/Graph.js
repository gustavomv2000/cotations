import React, {Component} from 'react';
import './Graph.css';
import Chart from 'chart.js';
import '../../Pages/RealTime';

export default class Graph extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            cot: {
                name: "",
                buy: "",
                sell: "",
                variation: "",
                cotDate: ""
            }
        };
        this.graph = this.graph.bind(this);
    }

    chartRef = React.createRef();

    componentDidMount() {

        console.log('Entrou');
        fetch('http://localhost/')
        .then(results => {
            return results.json();
        }).then(resultado => {
            this.setState({cot: resultado})
        }).then(() => {
            window.addEventListener('load', this.graph, false);
        });
        
    }

    graph = function() {
        console.log('graph')
        var arr = this.state.cot;

        var variation = [];
        var data = [];
        var firstCot = 1;
        
        for(var item of arr) {
            var date = new Date(item.cotDate);
            var days = date.getDate();
            days = days < 10 ? days = '0' + days: days; 
            var month = date.getMonth() + 1;
            month = month < 10 ? month = '0' + month: month;
            var hours = date.getHours();
            hours = hours < 10 ? hours = '0' + hours: hours;
            var min = date.getMinutes();
            min = min < 10 ? min = '0' + min: min;
            var strDate = days + '/' + month + ' ' + hours + ':' + min; 

            if(hours === 10 && min <= 10) {
                firstCot = item.sell
                console.log('Primeira Cotação: ' + firstCot + ' Horário: ' + strDate);
            }

            data.push(strDate);
            variation.push(item.variation/firstCot);
        }
        
        const myChartRef = this.chartRef.current.getContext('2d');
        
        
        new Chart(myChartRef, {
            theme: "light2",   
            interactivityEnabled: true,
            animationEnabled: true,
            responsive: true,
            type: 'line',
            data: {
                // labels: ['JAN','FEV','MAR','ABR', 'MAI','JUN','JUL','AGO','SET','OUT','NOV','DEZ'],
                // labels: ["2/7 14:13", "2/7 15:31", "2/7 15:31", "2/7 15:31", "2/7 15:31", "2/7 15:31", "2/7 15:31", "2/7 15:32", "2/7 15:32", "2/7 15:37", "2/7 15:54", "2/7 15:54", "3/7 9:54", "3/7 10:35", "3/7 10:35", "3/7 11:30"],
                labels: data,
                datasets: [
                    {
                        label: 'Cotação',
                        // data: [3.99,3.98,3.96,3.89,4.03,3.88,3.75,4.01,3.77,3.88,3.95,3.99,3.77],
                        data: variation,
                        borderColor: "#bae755",
                        pointBackgroundColor: "#55bae7",
                        pointBorderColor: "#55bae7",
                        pointHoverBackgroundColor: "#55bae7",
                        pointHoverBorderColor: "#55bae7",
                    }
                ],
                lineColor: 'black'
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontSize: 12,
                            fontStyle: 'bold',
                            fontColor: 'white',
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize: 10,
                            fontStyle: 'bold',
                            fontColor: 'white',
                        }
                    }],
                },
            }
        });
    }
        
    render() {
        return(
            <section className="graph">
                <div className="box-graph">
                    <header className="subHeaderG">
                        <h1 className="subTitleG" onClick={this.graph}>Gráfico Variação Dólar</h1>
                    </header>
                    <div className="graph-content">
                        <div className="tamGra">
                            <canvas
                                id="myChart"
                                ref={this.chartRef}
                            />
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

