import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.css';

import Header from './components/Header/Header';
import RealTime from './Pages/RealTime';
import Calculator from './Pages/Calculator';
import Footer from './components/Footer/Footer'
import Graph from './components/Graph/Graph';


function App() {
  return (
    <BrowserRouter>
      <Header />
        <section className="app">
          <Graph />
          <RealTime />
        </section>
        <div className="calc-div">
          <Calculator />
        </div>
        <div className="foot">
          <Footer />
        </div>
    </BrowserRouter>
  );
}

export default App;
