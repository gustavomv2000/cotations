import React from 'react';

import './Calculator.css';

export default class TextInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cot: {
                name: "",
                buy: "",
                sell: "",
                variation: "",
                cotDate: ""
            },
            currency: {
                dolar: "",
                real: ""
            },
        };
        this.calculationDolar = this.calculationDolar.bind(this);
        this.calculationReal = this.calculationReal.bind(this);
        this.cleanFields = this.cleanFields.bind(this);
    }

    componentDidMount() {
        console.log('Entrou calc');
        fetch('http://localhost/')
        .then(results => {
            return results.json();
        }).then(resultado => {
            this.setState({cot: resultado})
        });        
    }
    
    calculationDolar = function(e) {
        var arr = this.state.cot;
        var item = arr[arr.length - 1];
        var buy = item.buy;
        var dolar = e.target.value;
        this.setState({
            currency: {
                real: `R$ ${(buy * dolar).toFixed(2)}`
            }
        })
    }

    calculationReal = function(e) {
        var arr = this.state.cot;
        var item = arr[arr.length - 1];
        var buy = item.buy;
        var real = e.target.value;
        this.setState({
            currency: {
                dolar: `US$ ${(real / buy).toFixed(2)}`
            }
        })
    }

    cleanFields = function() {
        this.setState({
            currency: {
                dolar: "",
                real: ""
            }
        })
    }
    
    render() {
        return (
            <section className="header-calc">
                <div className="box-calc">
                    <div className="content-size">
                        <header className="subFormHeader">
                            <h1 className="subTitleForm">Conversão</h1>
                            <div id="teste" className="field">
                                <input
                                    type = "text"
                                    placeholder = "R$"
                                    onClick = {this.cleanFields}
                                    onChange = {this.calculationReal}
                                    value = {this.state.currency.real}
                                />
                            </div>
                            <p>&#8651;</p>
                            <div className="field">
                                <input
                                    type = "text"
                                    placeholder = "US$"
                                    onClick = {this.cleanFields}
                                    onChange={this.calculationDolar}
                                    value = {this.state.currency.dolar}
                                />
                            </div>
                        </header>
                    </div>
                </div>
            </section>
        )
    }
}