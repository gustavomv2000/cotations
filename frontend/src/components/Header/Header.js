import React from 'react';
import { Link } from 'react-router-dom';

import './Header.css';
import Coin from '../../img/coin.png';

export default function Header() {
    return(
    <section>
       <header className="header-principal">
        <Link to="/" style={{ textDecoration: 'none'}}>
          <h1>Cotações</h1>
        </Link>
        <div>  
          <img className="coin" alt="" src={Coin}/>  
        </div>
       </header>
    </section>
    )
}