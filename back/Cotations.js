const request = require("request");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fs = require("fs");
const mysql = require('mysql');

var url = "https://api.hgbrasil.com/finance?key=6814f41c";
var usd = {};

main();

async function main() {
    await createCsv();
    
    while(true) {
        var data = new Date();
        var day = data.getDate();
        var month = data.getMonth();
        var year = data.getFullYear();
        var strDate = `${month + 1}-${day}-${year}`;
        var openDate = new Date(`${strDate} 09:30:00`);
        var closeDate = new Date(`${strDate} 17:00:00`);
        var diferenceDate = openDate.getTime() - data.getTime();

        if((data > openDate) && (data <= closeDate)) {
            await getCoin();
            await sleep(10 * 60);
        }
        else if(diferenceDate > 0) {
            console.log('Mercado fechado no momento.');
            await sleep(diferenceDate/1000);
        }
        else {
            console.log('Mercado fechado até amanhã.');
            break;
        }
    }
}

async function getCoin() {
    request(url, function(error, response, body) {
        if (error) {
            console.log("error! ", error);
        } else {
            var date = new Date();
            usd = JSON.parse(body).results.currencies.USD;
            usd.date = date;
            appendCsv(usd);
            writeJson(JSON.stringify(usd));
            addToDatabase(usd);
        }
    })
}

async function createCsv() {
    const csvWriter = createCsvWriter({
        path: "Cotation.csv",
        header: [
            {id: "name", title: "Name"},
            {id: "buy", title: "Preço Compra"},
            {id: "sell", title: "Preço Venda"},
            {id: "variation", title: "Variação"},
            {date: "date", title: "Data"},
        ]
    });

    csvWriter
        .writeRecords("")
        .then(() => console.log("The CSV file was created succesfully"));
}

function addToDatabase(usd) {
    try {
        var sql = "INSERT INTO dollarCot (name, buy, sell, variation, cotDate) VALUES ?";  
        var values = [
            ['Dólar', usd.buy, usd.sell, usd.variation, usd.date]
        ];
        connection.query(sql, [values], function (err, result) {
            if(err) throw err;
        })
    }
    catch(e) {
        console.log(err)
    }
}

function appendCsv(usd) {
    var csv = jsonToCsv(usd);
    fs.appendFile("Cotation.csv", csv, function (err) {
        if(err) 
            console.log('Error to append');
        else
            console.log('CSV appended succesfully')
    })
}

function writeJson(usd) {
    if(!fs.existsSync('CotJson.json')) {
        fs.writeFile('CotJson.json', usd, function(err) {
            if(err)
                console.log('Erro');
            else
                console.log('JSON Created succesfully');
        });
    }
    else {
        fs.appendFile('CotJson.json', usd, function(err) {
            if(err)
                console.log('Erro');
            else
                console.log('JSON Appended succesfully');
        });
    }
}

function jsonToCsv(usd) {

    var array = [];
    array[0] = usd.name;
    array[1] = usd.buy;
    array[2] = usd.sell;
    array[3] = usd.variation;
    array[4] = usd.date;

    var str = '';

    array.forEach(item => {
        str += item + ',';
    });

    return str  + '\r\n';    
}

function sleep(time) {
    time = time * 1000;

    return new Promise(r => setTimeout(r, time));    
}

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'cotations'
});

connection.connect((err) => {
    if (err) throw err;
    console.log('Connected!');
});

